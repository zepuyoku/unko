//==============================================
//
// 試験
// Author : 佐藤諒佳
//
//===============================================
#define _CRT_SECURE_NO_WARNINGS			//scanfのエラー
//===============================================
//	インクルードファイル
//===============================================
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//===============================================
//	マクロ定義
//===============================================
#define MAX_DATA	(10)					//データの最大
//#define MIN_NUMBER						//最小の数
//#define MAX_NUMBER						//最大の数
#define PI (3.141592f)//円周率
//===============================================
//メイン関数
//===============================================
void main(void)
{
	int nCount;

	printf("1から4の数字を入力してください	>");
	scanf("%d", &nCount);
		
	switch (nCount)
	{
	case 1:			//ソート		
		int nCount;
		int nCount2;
		int nWork;
		int aData[MAX_DATA];

		srand((unsigned)time(NULL));
		for (nCount = 0; nCount < MAX_DATA; nCount++)
		{
			aData[nCount] = rand() % 100 + 1;
		}
		//入出力
		printf("ソート前\n");
		for (nCount = 0; nCount < MAX_DATA; nCount++)
		{
			printf("%3d\n", aData[nCount]);
		}
		for (nCount = 0; nCount < MAX_DATA - 1; nCount++)
		{
			for (nCount2 = nCount + 1; nCount2 < MAX_DATA; nCount2++)
			{
				if (aData[nCount] < aData[nCount2])
				{
					nWork = aData[nCount];
					aData[nCount] = aData[nCount2];
					aData[nCount2] = nWork;
				}
			}
		}
		printf("ソート後\n");
		for (nCount = 0; nCount < MAX_DATA; nCount++)
		{
			printf("%3d\n", aData[nCount]);
		}
		for (nCount = 0; nCount < MAX_DATA - 1; nCount++)
		{
			for (nCount2 = nCount + 1; nCount2 < MAX_DATA; nCount2++)
			{
				if (aData[nCount] < aData[nCount2])
				{
					nWork = aData[nCount];
					aData[nCount] = aData[nCount2];
					aData[nCount2] = nWork;
				}
			}
		}
		rewind(stdin);
		getchar();

		break;
	case 2:			//ランク

		break;
	case 3:			//ユークリッド
		int nData01, nData02;
		int nWork2;
		int	nFactor;

		//タイトル
		printf("+--------------------+\n");
		printf("| 最大公約数計算処理 |\n");
		printf("+--------------------+\n");

		//入力処理
		printf("1つ目の正整数--->");
		scanf("%d", &nData01);
		printf("2つ目の正整数--->");
		scanf("%d", &nData02);

		//最大公約数計算処理
		nWork2 = nData01;
		nFactor = nData02;

		//自然数を確認　入れ替え
		if (nData01<nData02)
		{
			nWork2 = nData01;
			nData01 = nData02;
			nData02 = nWork2;
		}

		//ユークリッドの互除法
		nFactor = nData01%nData02;
		while (nFactor != 0)
		{
			nData01 = nData02;
			nData02 = nFactor;
			nFactor = nData01%nData02;
		}
		//最大公約数表示
		printf("\n");
		printf("最大公約数は%dである。\n", nData02);

		//終了
		printf("\n");
		printf("+----------------------------+\n");
		printf("| 終了します                 |\n");
		printf("| (何かキーを押してください) |\n");
		printf("+----------------------------+\n");

		rewind(stdin);
		getchar();
		break;
		
	case 4:			//面積
		int nNumber;
		float fside01, fside02;//辺の長さ
		float fRadius;//半径
		float fArea;

		//タイトル
		printf("+--------------+\n");
		printf("| 面積計算処理 |\n");
		printf("+--------------+\n");


		//入力処理
		printf("\n");
		printf("図形の種類(正方形=1、長方形=2、円=3)--->");
		scanf("%d", &nNumber);
		//面積計算処理
		switch (nNumber)
		{
		case 1:
			printf("一辺の長さ--->");
			scanf("%f", &fside01);

			fArea = fside01*fside01;
			printf("面積は%.2f\n", fArea);
			break;

		case 2:
			printf("縦の長さ--->");
			scanf("%f", &fside01);
			printf("横の長さ--->");
			scanf("%f", &fside02);

			fArea = fside01*fside02;
			printf("面積は%.2f\n", fArea);
			break;

		case 3:
			printf("半径の長さ--->");
			scanf("%f", &fRadius);

			fArea = fRadius*fRadius*PI;
			printf("面積は%.2f\n", fArea);
		}
		//終了
		printf("\n");
		printf("+----------------------------+\n");
		printf("| 終了します                 |\n");
		printf("| (何かキーを押してください) |\n");
		printf("+----------------------------+\n");

		rewind(stdin);
		getchar();
		break;
	default:
		break;
	}
}