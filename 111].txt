//=============================================================================
//
// ２Ｄアクションゲーム
// Author :佐藤颯紀
//
//=============================================================================

#include "main.h"
#include "block.h"
#include "bg.h"
#include "player.h"
#include "input.h"
#include "enemy.h"



//======================================
//グローバル変数宣言
//======================================
LPDIRECT3DTEXTURE9 g_apTextureBlock[MAX_TYPE_BLOCK] = {};	//テクスチャへのポインタ
LPDIRECT3DVERTEXBUFFER9 g_pVtxBuffBlock = NULL;				//頂点バッファへのポインタ

BLOCK g_aBlock[MAX_BLOCK];		//ブロックの総数情報

int g_nNumBlock = 0;			//ブロックの総数


//======================================
//初期化処理
//======================================
void InitBlock(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	int nCntBlock;
	VERTEX_2D01*pVtx; //頂点情報へのポインタ


	 //テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,
		"block000.jpg",      //テクスチャの読み込み
		&g_apTextureBlock[0]);

	//オブジェクトの頂点バッファを生成
	pDevice->CreateVertexBuffer(
		sizeof(VERTEX_2D01)*NUM_VERTEX*MAX_BLOCK,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_2D,
		D3DPOOL_MANAGED,
		&g_pVtxBuffBlock,
		NULL);



	//頂点データ範囲をロックし、頂点バッファへのポインタを所得
	g_pVtxBuffBlock->Lock(0, 0, (void**)&pVtx, 0);


	//敵の情報を初期化
	for (nCntBlock = 0; nCntBlock < MAX_BLOCK; nCntBlock++)
	{
		g_aBlock[nCntBlock].pos = D3DXVECTOR3(BLOCK_SIZE_X, BLOCK_SIZE_Y, 0.0f);
		g_aBlock[nCntBlock].move = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
		g_aBlock[nCntBlock].nType = 0;
		g_aBlock[nCntBlock].bUse = false;


		//テクスチャ座標の設定
		pVtx[0].tex = D3DXVECTOR2(0.0f, 0.0f);
		pVtx[1].tex = D3DXVECTOR2(1.0f, 0.0f);
		pVtx[2].tex = D3DXVECTOR2(0.0f, 1.0f);
		pVtx[3].tex = D3DXVECTOR2(1.0f, 1.0f);

		//頂点座標の設定(右回りで設定する)
		pVtx[0].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
		pVtx[1].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
		pVtx[2].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);
		pVtx[3].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);

		//rhwの設定値は1,0で固定
		pVtx[0].rhw = 1.0f;
		pVtx[1].rhw = 1.0f;
		pVtx[2].rhw = 1.0f;
		pVtx[3].rhw = 1.0f;

		//頂点カラーの設定(0~255の数値で設定)
		pVtx[0].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //r,g,b,aは0~255の範囲で決めること
		pVtx[1].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //r:レッド g:グリーン　b:ブルー
		pVtx[2].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //aは、透明度を表している
		pVtx[3].col = D3DCOLOR_RGBA(255, 255, 255, 255);

		/*g_aEnemy[nCntEnemy].pos = D3DXVECTOR3(ENEMY_POS_X, ENEMY_POS_Y, 0);*/
		pVtx += 4; //頂点データのポインタを４つにする
	}
	//頂点データをアンロック
	g_pVtxBuffBlock->Unlock();

	//ブロックの総数をクリア
	g_nNumBlock = 0;
}


//===========================================
//終了処理
//===========================================
void UninitBlock(void)
{
	int nCntTex;

	//テクスチャの開放
	for (nCntTex = 0; nCntTex < MAX_TYPE_BLOCK; nCntTex++)
	{
		if (g_apTextureBlock[nCntTex] != NULL)
		{
			g_apTextureBlock[nCntTex]->Release();
			g_apTextureBlock[nCntTex] = NULL;
		}
	}
	//頂点バッファの開放
	if (g_pVtxBuffBlock != NULL)
	{
		g_pVtxBuffBlock->Release();
		g_pVtxBuffBlock = NULL;
	}
}

//=============================================
//描画処理
//=============================================
void DrawBlock(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	int nCntBlock;
	//頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource(0, g_pVtxBuffBlock, 0, sizeof(VERTEX_2D01));

	//頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_2D);


	//ポリゴンの描画
	for (nCntBlock = 0; nCntBlock < MAX_BLOCK; nCntBlock++)
	{
		if (g_aBlock[nCntBlock].bUse == true)    //ブロックが使用中なら描画
		{
			//テクスチャの設定
			pDevice->SetTexture(0, g_apTextureBlock[g_aBlock[nCntBlock].nType]);
			pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, nCntBlock * 4, NUM_POLYGON);



		}
	}
}
//ブロックの取得
BLOCK*GetBlock(void)
{
	return &g_aBlock[0];
}

//=====================================================
//更新処理
//=====================================================
void UpdateBlock(void)
{
	VERTEX_2D01*pVtx; //頂点情報へのポインタ
	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	int nCntBlock;
	PLAYER*pPlayer;

	pPlayer = GetPlayer();

	//頂点座標の設定
	g_pVtxBuffBlock->Lock(0, 0, (void**)&pVtx, 0);

	//ブロックを取得
	for (nCntBlock = 0; nCntBlock < MAX_BLOCK; nCntBlock++)
	{
		if (pPlayer->Sclool == true) //敵が使用されている
		{
			/*g_aBlock[nCntBlock].pos -= g_aBlock[nCntBlock].move;*/
			g_aBlock[nCntBlock].pos.x -= 5.0f;

			//頂点座標の設定(右回りで設定する)
			pVtx[0].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[1].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[2].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[3].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);
		}

		if (pPlayer->backSclool == true)
		{
			g_aBlock[nCntBlock].pos.x += 5.0f;
			//頂点座標の設定(右回りで設定する)
			pVtx[0].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[1].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[2].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[3].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);
		}

		pVtx += 4; //頂点データのポインタを４つ分進める
		
	}

	//頂点データをアンロックする
	g_pVtxBuffBlock->Unlock();

}


//=====================================================
//ブロックの更新処理
//=====================================================
void SetBlock(D3DXVECTOR3 pos, int nType)
{
	VERTEX_2D01*pVtx; //頂点情報へのポインタ

	int nCntBlock;

	//頂点座標の設定
	g_pVtxBuffBlock->Lock(0, 0, (void**)&pVtx, 0);

	for (nCntBlock = 0; nCntBlock < MAX_BLOCK; nCntBlock++)
	{
		if (g_aBlock[nCntBlock].bUse == false) //弾が使用されていない場合
		{
			//位置を設定
			g_aBlock[nCntBlock].pos = pos;
			g_aBlock[nCntBlock].nType = nType;
			//移動量を設定
			g_aBlock[nCntBlock].bUse = true; //使用してる状態にする


			//pVtx += nCntBullet * 4;
			//頂点座標の設定(右回りで設定する)
			pVtx[0].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[1].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((-BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[2].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((-BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);
			pVtx[3].pos = D3DXVECTOR3(g_aBlock[nCntBlock].pos.x + ((BLOCK_SIZE_X) / 2.0f), g_aBlock[nCntBlock].pos.y + ((BLOCK_SIZE_Y) / 2.0f), 0.0f);


			g_nNumBlock++;		//ブロックの総数をカウント
			break;
		}


		pVtx += 4; //頂点データのポインタを４つ分進める
	}
	//頂点データをアンロックする
	g_pVtxBuffBlock->Unlock();
	

}



//=====================================================
//地面との当たり判定
//=====================================================
bool CollisionBlock(D3DXVECTOR3*pPos, D3DXVECTOR3*pPosOld, D3DXVECTOR3*pMove, D3DXVECTOR3 size)
{
	//BLOCK*pBlock;
	int nCntBlock;

	bool bLand = false;

	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	PLAYER*pPlayer;
	ENEMY*pEnemy;

	//自機を取得
	pPlayer = GetPlayer();
	pEnemy = GetEnemy();

	//ブロックを取得
	for (nCntBlock = 0; nCntBlock < MAX_BLOCK; nCntBlock++)
	{
			g_aBlock[nCntBlock].bLand = false;

			//==================================
			//プレイヤーとブロックの当たり判定
			//==================================

			if (pPlayer->pos.y - (PLAYER_SIZE / 2) <= g_aBlock[nCntBlock].pos.y + (BLOCK_SIZE_Y / 2)-4
				&& g_aBlock[nCntBlock].pos.y - (BLOCK_SIZE_Y/2)+7 <= pPlayer->pos.y + (PLAYER_SIZE / 2))
			{
				//プレイヤーの左側がブロックの右側にふれた
				if (pPlayer->pos.x <= g_aBlock[nCntBlock].pos.x + (BLOCK_SIZE_X / 2) + (PLAYER_SIZE / 2)
					&& pPosOld->x >= g_aBlock[nCntBlock].pos.x + (BLOCK_SIZE_X / 2) + (PLAYER_SIZE / 2))
				{
					pPos->x = g_aBlock[nCntBlock].pos.x + (BLOCK_SIZE_X / 2) + (PLAYER_SIZE / 2);
					pMove->x = 0.0f;
					bLand = true;
					pPlayer->backSclool = false;

				}

				//プレイヤーの右側がブロックの左側にふれた
				if (pPlayer->pos.x >= g_aBlock[nCntBlock].pos.x - (BLOCK_SIZE_X / 2) - (PLAYER_SIZE / 2)
					&& pPosOld->x <= g_aBlock[nCntBlock].pos.x - (BLOCK_SIZE_X / 2) - (PLAYER_SIZE / 2))
				{
					pPos->x = g_aBlock[nCntBlock].pos.x - (BLOCK_SIZE_X / 2) - (PLAYER_SIZE / 2);
					pMove->x = 0.0f;
					bLand = true;
					pPlayer->Sclool = false;
				}
			}

			if(pPlayer->pos.x - (PLAYER_SIZE / 2) <= g_aBlock[nCntBlock].pos.x + (BLOCK_SIZE_X / 2)-2
				&& g_aBlock[nCntBlock].pos.x - (BLOCK_SIZE_X / 2)+2 <= pPlayer->pos.x + (PLAYER_SIZE / 2))
			{

				//プレイヤーの上側がブロックの下側にふれた
				if (pPlayer->pos.y <= g_aBlock[nCntBlock].pos.y + (BLOCK_SIZE_Y / 2) + (PLAYER_SIZE / 2)
					&&pPosOld->y >= g_aBlock[nCntBlock].pos.y + (BLOCK_SIZE_Y / 2) + (PLAYER_SIZE / 2))
				{
					pPos->y = g_aBlock[nCntBlock].pos.y + (BLOCK_SIZE_Y / 2) + (PLAYER_SIZE / 2);
					pMove->y = 0.0f;
					bLand = true;
				}

				//プレイヤーの下側がブロックの上側にふれた
				if (pPlayer->pos.y >= g_aBlock[nCntBlock].pos.y - (BLOCK_SIZE_Y / 2) - (PLAYER_SIZE / 2)
					&&pPosOld->y <= g_aBlock[nCntBlock].pos.y - (BLOCK_SIZE_Y / 2) - (PLAYER_SIZE / 2))
				{
					pPos->y = g_aBlock[nCntBlock].pos.y - (BLOCK_SIZE_Y / 2) - (PLAYER_SIZE / 2);
					pMove->y = 0.0f;
					bLand = true;
				}
			}
		}
	return bLand;
}

-----------------------------------------------------------------------------------------------------------------------------------------------


//=============================================================================
//
// ２Ｄアクションゲーム
// Author :佐藤颯紀
//
//=============================================================================


//================
//ポリゴンの処理
//================

#include "main.h"
#include "bg.h"
#include "player.h"
#include "block.h"
#include "input.h"
//=================
//グローバル変数
//=================
LPDIRECT3DTEXTURE9 g_pTexturePolygon = NULL; //テクスチャへのポインタ
LPDIRECT3DVERTEXBUFFER9 g_pVtxBuffPolygon = NULL; //頂点バッファへのポインタ

PLAYER g_player;


//D3DXVECTOR3 posBullet;
int g_nCounterAnim; //アニメーションカウンター
int g_nPatternAnim; //アニメーションパターンNo



//=====================
//初期化処理
//=====================
void InitPlayer(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();

	g_player.pos = D3DXVECTOR3(POLYGON_POS_X, POLYGON_POS_Y, 0);
	g_player.move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	g_player.bUse = true;
	g_player.bJump = false;
	g_nCounterAnim = 0;
	g_nPatternAnim = 0;

	//テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,
		"player000.png", //ファイルの読み込み
		&g_pTexturePolygon);

	//頂点バッファの生成
	pDevice->CreateVertexBuffer(
		sizeof(VERTEX_2D01)*NUM_VERTEX,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_2D,
		D3DPOOL_MANAGED,
		&g_pVtxBuffPolygon,
		NULL);

	VERTEX_2D01*pVtx; //頂点情報へのポインタ

	//頂点データ範囲をロックし、頂点バッファへのポインタを所得
	g_pVtxBuffPolygon->Lock(0, 0, (void**)&pVtx, 0);


	//テクスチャ座標の設定
	pVtx[0].tex = D3DXVECTOR2(g_nPatternAnim*0.25f, 0.0f);
	pVtx[1].tex = D3DXVECTOR2(g_nPatternAnim*0.25f + 0.25f, 0.0f);
	pVtx[2].tex = D3DXVECTOR2(g_nPatternAnim*0.25f, 0.5f);
	pVtx[3].tex = D3DXVECTOR2(g_nPatternAnim*0.25f + 0.25f, 0.5f);


	//頂点座標の設定(右回りで設定する)
	pVtx[0].pos = D3DXVECTOR3(g_player.pos.x + ((-PLAYER_SIZE) / 2), g_player.pos.y + ((-PLAYER_SIZE) / 2), 0.0f);
	pVtx[1].pos = D3DXVECTOR3(g_player.pos.x + ((PLAYER_SIZE) / 2), g_player.pos.y + ((-PLAYER_SIZE) / 2), 0.0f);
	pVtx[2].pos = D3DXVECTOR3(g_player.pos.x + ((-PLAYER_SIZE) / 2), g_player.pos.y + ((PLAYER_SIZE) / 2), 0.0f);
	pVtx[3].pos = D3DXVECTOR3(g_player.pos.x + ((PLAYER_SIZE) / 2), g_player.pos.y + ((PLAYER_SIZE) / 2), 0.0f);

	//rhwの設定値は1,0で固定
	pVtx[0].rhw = 1.0f;
	pVtx[1].rhw = 1.0f;
	pVtx[2].rhw = 1.0f;
	pVtx[3].rhw = 1.0f;

	//頂点カラーの設定(0~255の数値で設定)
	pVtx[0].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //r,g,b,aは0~255の範囲で決めること
	pVtx[1].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //r:レッド g:グリーン　b:ブルー
	pVtx[2].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //aは、透明度を表している
	pVtx[3].col = D3DCOLOR_RGBA(255, 255, 255, 255);

	//頂点データをアンロック
	g_pVtxBuffPolygon->Unlock();

}
//=============================
//描画処理
//=============================
void DrawPlayer(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	//int nCntPlayer;
	//頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_2D);
	//頂点バッファをデバイスのデータストリームにバインド
	pDevice->SetStreamSource(0, g_pVtxBuffPolygon, 0, sizeof(VERTEX_2D01));
	//頂点フォーマットの設定
	pDevice->SetTexture(0, g_pTexturePolygon);


	//ポリゴンの描画
	if (g_player.bUse == true)
	{
		//弾が使用中なら描画
		pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, NUM_POLYGON);
	}
}

//自機の取得
PLAYER*GetPlayer(void)
{
	return &g_player;
}



//=============================================================================
// 終了処理
//=============================================================================
void UninitPlayer(void)
{

	//テクスチャの開放
	if (g_pTexturePolygon != NULL)
	{
		g_pTexturePolygon->Release();
		g_pTexturePolygon = NULL;
	}
	//頂点バッファの開放
	if (g_pVtxBuffPolygon != NULL)
	{
		g_pVtxBuffPolygon->Release();
		g_pVtxBuffPolygon = NULL;
	}
}

//=============================================================================
// 更新処理
//=============================================================================
void UpdatePlayer(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	VERTEX_2D01 *pVtx; //頂点情報へのポインタ

	BLOCK *pBlock;

	static float velocity = 0.0f;
	g_nCounterAnim++; //カウンタ加算

	//頂点データ範囲をロックし、頂点バッファへのポインタを所得
	g_pVtxBuffPolygon->Lock(0, 0, (void**)&pVtx, 0);

	//switch (g_player.state)
	//{
	//case PLAYERSTATE_NORMAL:
	//	break;

	//case PLAYERSTATE_DAMAGE:
	//	g_player.nCountState--;
	//	if (g_player.nCountState <= 0)
	//	{
	//		//頂点カラーの設定(0~255の数値で設定)
	//		pVtx[0].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //r,g,b,aは0~255の範囲で決めること
	//		pVtx[1].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //r:レッド g:グリーン　b:ブルー
	//		pVtx[2].col = D3DCOLOR_RGBA(255, 255, 255, 255);  //aは、透明度を表している
	//		pVtx[3].col = D3DCOLOR_RGBA(255, 255, 255, 255);
	//	}
	//	break;

	//case PLAYERSTATE_DEATH:
	//	g_player.nCountState--;
	//	if (g_player.nCountState <= 0)
	//	{
	//		SetGameState(GAMESTATE_ENDPLAYER);
	//	}
	//	return;
	//	break;
	//}


	//=========================
	//キーボード入力
	//=========================
	if (g_player.bUse == true)
	{
		//Aのキーが押されたかどうか
		if (GetKeyboardPress(DIK_A))
		{
			g_player.move.x -= (1.5f);

			g_pVtxBuffPolygon->Lock(0, 0, (void**)&pVtx, 0);

			//テクスチャ座標の設定
			pVtx[0].tex = D3DXVECTOR2(g_nPatternAnim*0.25f, 0.5f);
			pVtx[1].tex = D3DXVECTOR2(g_nPatternAnim*0.25f + 0.25f, 0.5f);
			pVtx[2].tex = D3DXVECTOR2(g_nPatternAnim*0.25f, 1.0f);
			pVtx[3].tex = D3DXVECTOR2(g_nPatternAnim*0.25f + 0.25f, 1.0f);

			//頂点データをアンロックする
			g_pVtxBuffPolygon->Unlock();

			g_player.backSclool = false;
			if (g_player.pos.x - (PLAYER_SIZE / 2) < 300)
			{
				g_player.backSclool = true;
				g_player.pos.x - (PLAYER_SIZE / 2) < 300;


				
			}
		}

		else
		{
			g_player.backSclool = false;
		}

		//Dのキーが押されたかどうか
		if (GetKeyboardPress(DIK_D))
		{
			g_player.move.x += (1.5f);

			g_pVtxBuffPolygon->Lock(0, 0, (void**)&pVtx, 0);

			//テクスチャ座標の設定
			pVtx[0].tex = D3DXVECTOR2(g_nPatternAnim*0.25f, 0.0f);
			pVtx[1].tex = D3DXVECTOR2(g_nPatternAnim*0.25f + 0.25f, 0.0f);
			pVtx[2].tex = D3DXVECTOR2(g_nPatternAnim*0.25f, 0.5f);
			pVtx[3].tex = D3DXVECTOR2(g_nPatternAnim*0.25f + 0.25f, 0.5f);

			//頂点データをアンロックする
			g_pVtxBuffPolygon->Unlock();

			g_player.Sclool = false;
			if (g_player.pos.x + (PLAYER_SIZE / 2) > 800)
			{
				g_player.Sclool = true;
				g_player.pos.x + (PLAYER_SIZE / 2) > 800;


				
			}
		}
		else
		{
			g_player.Sclool = false;
		}

		//スペースキーが押されたかどうか
		if(GetKeyboardTrigger(DIK_SPACE))
		{
			if (g_player.bJump == false)
			{
				g_player.move.y -= (90.0f);
				g_player.bJump = true;
			}

		}

		//===================================================
		//壁判定
		//===================================================
		if (g_player.pos.x - (PLAYER_SIZE / 2)<300)
		{//画面が左端に達したとき
			g_player.pos.x = 300 + (PLAYER_SIZE / 2);
		}

		if (g_player.pos.x + (PLAYER_SIZE / 2) > 800)
		{//画面が右端に達したとき
			g_player.pos.x = 800 - (PLAYER_SIZE / 2);
		}

		//if (g_player.pos.y - (PLAYER_SIZE / 2) < 300)
		//{//画面が上端に達したとき
		//	g_player.pos.y = 0 + (PLAYER_SIZE / 2);
		//}

		//if (g_player.pos.y - (PLAYER_SIZE / 2) > SCREEN_HEIGHT)
		//{//画面が下端に達したとき
		//	g_player.pos.y = SCREEN_HEIGHT - (PLAYER_SIZE / 2);
		//	g_player.bJump = false;
		//}

	


		//=================================
		//ブロックの当たり判定
		//=================================
		
		g_player.posOld = g_player.pos;

		
		//=================================
		//重力・慣性など
		//=================================
		//重力
		g_player.move.y += (60.0f - g_player.move.y)*RATE_MOVE;

		//位置情報
		g_player.pos.x += g_player.move.x;
		g_player.pos.y += g_player.move.y;

		if (g_player.pos.y > SCREEN_HEIGHT - 25)
		{
			g_player.pos.y = SCREEN_HEIGHT - 25;
			g_player.move.y = 0.0f;
			g_player.bJump = false;
		}
		//=============
		//慣性
		//=============
		g_player.move.x += (0.0f - g_player.move.x)*RATE_MOVE;
		/*g_player.move.y += (0.0f - g_player.move.y)*RATE_MOVE;*/


		if (g_nCounterAnim >= 8)
		{
			g_nCounterAnim = 0;//カンターを初期値を戻す
			g_nPatternAnim++;

			if (g_nPatternAnim >= 8)
			{
				g_nPatternAnim = 0;
			}
		}
		//ブロックとの当たり判定
		if (CollisionBlock(&g_player.pos, &g_player.posOld, &g_player.move, D3DXVECTOR3(PLAYER_WIDTH_COLLISION, PLAYER_HEIGHT, 0.0f)) == true)//着地した場合
		{
			g_player.bJump = false;
		}

		//頂点データをアンロックする
		g_pVtxBuffPolygon->Unlock();

	}


	g_pVtxBuffPolygon->Lock(0, 0, (void**)&pVtx, 0);


	//頂点座標の設定
	pVtx[0].pos = D3DXVECTOR3(g_player.pos.x + ((-PLAYER_SIZE) / 2), g_player.pos.y + ((-PLAYER_SIZE) / 2), 0.0f);
	pVtx[1].pos = D3DXVECTOR3(g_player.pos.x + ((PLAYER_SIZE) / 2), g_player.pos.y + ((-PLAYER_SIZE) / 2), 0.0f);
	pVtx[2].pos = D3DXVECTOR3(g_player.pos.x + ((-PLAYER_SIZE) / 2), g_player.pos.y + ((PLAYER_SIZE) / 2), 0.0f);
	pVtx[3].pos = D3DXVECTOR3(g_player.pos.x + ((PLAYER_SIZE) / 2), g_player.pos.y + ((PLAYER_SIZE) / 2), 0.0f);
	//頂点データをアンロック
	g_pVtxBuffPolygon->Unlock();
}



////=============================
////ダメージ設定
////=============================
//void HitPlayer(int nDamage)
//{
//
//	VERTEX_2D01*pVtx;
//
//	//ライフを減らす
//	g_player.nLife -= nDamage;
//
//	//頂点データ範囲をロックし、頂点バッファへのポインタを所得
//	g_pVtxBuffPolygon->Lock(0, 0, (void**)&pVtx, 0);
//
//	if (g_player.nLife <= 0)
//	{
//		//爆発生成
//		SetExplosion(D3DXVECTOR3(g_player.pos.x, g_player.pos.y, 0.0f), D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
//
//		g_player.bUse = false; //使用しない状態にする
//
//		g_player.state = PLAYERSTATE_DEATH;		//死亡状態に遷移
//
//		g_player.nCountState = 60;
//	}
//	else
//	{
//		//ダメージ状態に遷移
//		g_player.state = PLAYERSTATE_DAMAGE;		//ダメージ状態に遷移
//		g_player.nCountState = 5;
//
//		//頂点座標の設定
//		pVtx[0].pos = D3DXVECTOR3(g_player.pos.x + ((-PLAYER_SIZE) / 2.0f), g_player.pos.y + ((-PLAYER_SIZE) / 2.0f), 0.0f);
//		pVtx[1].pos = D3DXVECTOR3(g_player.pos.x + ((PLAYER_SIZE) / 2.0f), g_player.pos.y + ((-PLAYER_SIZE) / 2.0f), 0.0f);
//		pVtx[2].pos = D3DXVECTOR3(g_player.pos.x + ((-PLAYER_SIZE) / 2.0f), g_player.pos.y + ((PLAYER_SIZE) / 2.0f), 0.0f);
//		pVtx[3].pos = D3DXVECTOR3(g_player.pos.x + ((PLAYER_SIZE) / 2.0f), g_player.pos.y + ((PLAYER_SIZE) / 2.0f), 0.0f);
//
//		//頂点カラーの設定(0~255の数値で設定)
//		pVtx[0].col = D3DCOLOR_RGBA(255, 0, 0, 255);  //r,g,b,aは0~255の範囲で決めること
//		pVtx[1].col = D3DCOLOR_RGBA(255, 0, 0, 255);  //r:レッド g:グリーン　b:ブルー
//		pVtx[2].col = D3DCOLOR_RGBA(255, 0, 0, 255);  //aは、透明度を表している
//		pVtx[3].col = D3DCOLOR_RGBA(255, 0, 0, 255);
//
//	}
//	//頂点データをアンロックする
//	g_pVtxBuffPolygon->Unlock();
//}