//=============================================================================
//
// タイトル画面
// Author :佐藤颯紀
//
//=============================================================================

#include "game.h"
#include "main.h"
#include "player.h"
#include "bullt.h"
#include "explosion.h"
#include "enemy.h"
#include "bg.h"

//===================================
//グローバル変数宣言
//===================================
GAMESTATE g_gameState = GAMESTATE_NONE;		//ゲーム状態
int g_nCounterGameState = 0;				//ゲーム状態管理カウンタ


//===================================
//初期化処理
//===================================
void InitGame(void)
{
	g_gameState = GAMESTATE_NORMAL;		//通所状態に設定
	g_nCounterGameState = 0;

	bgInitPolygon();    //背景の初期化処理
	InitPlayer();		//プレイヤーの初期化処理
	InitBullet();		//弾の初期化処理
	InitExplosion();	//爆発の初期化処理
	InitEnemy();		//敵の初期化処理
	SetEnemy(D3DXVECTOR3(300.0f, 100.0f, 0.0f), 0);
	SetEnemy(D3DXVECTOR3(500.0f, 100.0f, 0.0f), 0);
}

//===================================
//終了処理
//===================================
void UninitGame(void)
{
	bgUninitPolygon();
	UninitBullet();
	UninitEnemy();
	UninitExplosion();
	UninitPlayer();

}
//===================================
//描画処理
//===================================
void DrawGame(void)
{
	bgDrawPolygon();
	DrawPlayer();
	DrawBullet();
	DrawExplosion();
	DrawEnemy();
}


//===================================
//更新処理
//===================================
void UpdateGame(void)
{
	switch (g_gameState)
	{
	case GAMESTATE_NORMAL:
		break;

	case GAMESTATE_END:
		
			SetMode(MODE_RESULT);
		
		break;
	}

	bgUpdataPolygon();
	UpdataPlayer();
	UpdateBullet();
	UpdateExplosion();
	UpdateEnemy();
}


//====================================
//ゲーム状態の設定
//====================================
void SetGameState(GAMESTATE state)
{
	g_gameState = state;
	g_nCounterGameState = 0;
}

//====================================
//ゲーム状態の取得
//====================================
GAMESTATE GetGameState(void)
{
	return g_gameState;
}